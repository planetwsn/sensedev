#!/bin/bash

set -eu -o pipefail

sudo -n true
test $? -eq 0 || exit 1 "you should have sudo privileges to run this script"

echo "Checking for updates..."
sudo apt-get update --allow-releaseinfo-change -y && sudo apt-get upgrade -y

echo "Installing mysql software..."
#sudo gdebi mariadb-server-10.0.deb mariadb-client-10.0.deb phpmyadmin.deb python-mysqldb.deb
sudo apt-get install mariadb-server-10.0 mariadb-client-10.0 python-dev apache2 php -y --fix-missing
sudo DEBIAN_FRONTEND=noninteractive sudo apt install phpmyadmin -y --fix-missing

sudo pip install mysqlclient
sudo apt -y autoremove

echo "Include /etc/phpmyadmin/apache.conf" | sudo tee -a /etc/apache2/apache2.conf > /dev/null
sudo /etc/init.d/apache2 restart

echo ""
echo "Done."
